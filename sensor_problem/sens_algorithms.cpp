// Copyright 2020, Grigorios Loukides
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Takuya Akiba nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <stack>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <time.h> // clock
#include <math.h>
#include <memory> // auto_ptr
#include "jlog.h"
#include "tools.h"
#include "mt19937ar.c"
#include <random>

using namespace std;
using namespace jlog_internal;
typedef long long LL;
typedef unsigned long long ULL;

//Provided by the authors of http://papers.nips.cc/paper/5709-monotone-k-submodular-function-maximization-with-size-constraints
double entropy(vector<vector<vector<int> > > &vals, vector<pair<int, int> > &S,
		int n_sensors, int n_ticks, int n_pos) {
	map<vector<int>, int> C;
	for (int t = 0; t < n_ticks; t++) {
		vector<int> x;
		for (int i = 0; i < S.size(); i++) {
			int p = S[i].first, s = S[i].second;
			x.push_back(vals[s][t][p]);
		}
		C[x]++;
	}
	double H = 0;
	for (auto it = C.begin(); it != C.end(); it++) {
		double pr = 1.0 * it->second / n_ticks;
		H += -pr * log(pr);
	}
	return H;
}

//Provided by the authors of http://papers.nips.cc/paper/5709-monotone-k-submodular-function-maximization-with-size-constraints
double mutual_information(vector<vector<vector<int> > > &vals,
		vector<pair<int, int> > &S, int n_sensors, int n_ticks, int n_pos) {
	// <p,s>
	// U = S \cup O
	vector<pair<int, int> > O, U;
	set<pair<int, int> > Sset(S.begin(), S.end());
	for (int p = 0; p < n_pos; p++) {
		for (int s = 0; s < n_sensors; s++) {
			pair<int, int> ps = make_pair(p, s);
			if (!Sset.count(ps)) {
				O.push_back(ps);
			}
			U.push_back(ps);
		}
	}

	double HS = entropy(vals, S, n_sensors, n_ticks, n_pos);
	return HS;
}

double lazy_greedy_sandwich_h(vector<vector<vector<int> > > &vals, int k, int tick, int n,
		int budget, double beta, double w1, double w2, double w3, vector<pair<int,int> >& sol,double& sol_spread, double& sol_cost) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}

	int B = budget;

	int A1=0;
	int A2=0;
	int A3=0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	int sum_num = 0;
	double max_H=-1.0;
	double max_cost=-1.0;
	double max_ratio=-1.0;

	int  size_of_current_best=0;

	for (int j = 0; j < B; j++) {
		pair<int, int> next;
		double start = get_current_time_sec();

		double old_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

		for (int num = 0;; num++)
		{
			double now = get_current_time_sec();
			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				sum_num += num;
				break;
			}
			vector<pair<int, int> > T(S);
			T.push_back(s);

			double new_denom=0.0;

			if(s.second	==0)
				new_denom=w1*pow(A1+1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);
			else if(s.second==1)
				new_denom=w1*pow(A1,beta)+w2*pow(A2+1,beta)+w3*pow(A3,beta);
			else if(s.second==2)
				new_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3+1,beta);

			double H1 = mutual_information(vals, T, k, tick, n)/new_denom;
			double H2 = mutual_information(vals, S, k, tick, n)/old_denom;
			que.push(make_pair(H1 - H2, make_pair(s, j)));
		}
		S.push_back(next);
		used[next.first] = true;


		if(next.second	==0)
			A1++;
		else if(next.second==1)
			A2++;
		else if(next.second==2)
			A3++;

		double H=mutual_information(vals, S, k, tick, n);
		double cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);
		
		 if(H/cost > max_ratio)
                {
                        max_H=H;
                        max_cost=cost;
                        max_ratio = max_H/max_cost;
                        size_of_current_best=j+1;
			sol=S;
			sol_spread=H;
			sol_cost=cost;

                }
	}
	JLOG_ADD("sum-num-of-eval", sum_num);

	return sol_spread/sol_cost;
}

double lazy_greedy_sandwich_g(vector<vector<vector<int> > > &vals, int k, int tick, int n,
		int budget, double beta, double w1, double w2, double w3, vector<pair<int,int> >& sol,double& sol_spread, double& sol_cost) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}

	int B = budget;

	int A1=0;
	int A2=0;
	int A3=0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	int sum_num = 0;
	int size_of_current_best=0;
	double max_H=-1.0;

	for (int j = 0; j < B; j++) {
		pair<int, int> next;
		double start = get_current_time_sec();

		double old_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

		for (int num = 0;; num++)
		{
			double now = get_current_time_sec();
			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				sum_num += num;
				break;
			}
			vector<pair<int, int> > T(S);
			T.push_back(s);

			double H1 = mutual_information(vals, T, k, tick, n);
			double H2 = mutual_information(vals, S, k, tick, n);
			que.push(make_pair(H1 - H2, make_pair(s, j)));
		}
		S.push_back(next);
		used[next.first] = true;

		if(next.second	==0)
			A1++;
		else if(next.second==1)
			A2++;
		else if(next.second==2)
			A3++;
		
		double entropy= mutual_information(vals, S, k, tick, n);
		double cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

		  if(entropy/cost > max_H)
                {
                        max_H=entropy/cost;                                                
                        size_of_current_best=j+1;
			sol=S;
			sol_spread=max_H;
                }
	}
	JLOG_ADD("sum-num-of-eval", sum_num);
	sol_cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

	return sol_spread/sol_cost;
}

void k_greed_ratio(vector<vector<vector<int> > > &vals, int k, int tick, int n,
		int budget, double beta, double w1, double w2, double w3 ) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}

	int B = budget;
    //keeps sizes of A_is
	int A1=0;
	int A2=0;
	int A3=0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	int sum_num = 0;

	double max_ratio=0.0, max_cost=0.0, max_H=0.0;
	int size_of_current_best=0;

	for (int j = 0; j < B; j++) {

		//node,j
		pair<int, int> next;

		double start = get_current_time_sec();

		double old_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

		for (int num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				sum_num += num;
				break;
			}
			vector<pair<int, int> > T(S);
			T.push_back(s);

			double ratio_denom=0.0;      //this is (Cost(new) - Cost(old))
			if(s.second	==0)
				ratio_denom=w1*(pow(A1+1,beta)-pow(A1,beta)); //the rest are cancelled out
			else if(s.second==1)
				ratio_denom=w2*(pow(A2+1,beta)-pow(A2,beta)); //the rest are cancelled out
			else if(s.second==2)
				ratio_denom=w3*(pow(A3+1,beta)-pow(A3,beta)); //the rest are cancelled out

			double H1 = mutual_information(vals, T, k, tick, n);
			double H2 = mutual_information(vals, S, k, tick, n);
			que.push(make_pair((H1 - H2)/ratio_denom, make_pair(s, j)));
		}
		//add a new node
		S.push_back(next);
		used[next.first] = true;

		double H = mutual_information(vals, S, k, tick, n);
		if(next.second	==0)
			A1++;
		else if(next.second==1)
			A2++;
		else if(next.second==2)
			A3++;
		double cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

		if(H/cost > max_ratio)
		{
			max_H=H;
			max_cost=cost;
			max_ratio = max_H/max_cost;
			size_of_current_best=j+1;
		}
	}
	JLOG_ADD("sum-num-of-eval", sum_num);
	cout<<"Solution size="<<size_of_current_best<<" H="<<max_H<<" cost="<<max_cost<<" H/cost="<<max_ratio<<endl;
}

void k_stochastic_greed_ratio(vector<vector<vector<int> > > &vals, int k, int tick, int n,
		int budget, double beta, double w1, double w2, double w3, double delta )
{
	int B=budget;
	vector<int> V(n);
		for (int i = 0; i < n; i++) {
			V[i] = i;
		}
		Xorshift xs(0);
		vector<bool> used(n);
		vector<pair<int, int> > S;

		// <place, sensor> -> gain
		vector<vector<double> > gains(n);
		for (int i = 0; i < n; i++) {
			gains[i].resize(k);
			gains[i].assign(k, 1e12);
		}

	double max_ratio=0.0, max_H=0.0, max_cost=0.0;
	int size_of_current_best=0;

	int A1=0;
	int A2=0;
	int A3=0;

	int sum_num = 0;
	for (int j = 1; j <= B; j++)
	{
			vector<int> R;
			int next_e = -1, next_z = -1;
			double next_gain = -1e12;
			int num = 0;

			//our sample size is different
			int ts=-1;

			if(log(budget/delta)>=budget)
				ts=n;
			else
				ts=1.0*ceil(log(budget/delta));

			double old_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

			for (int at = n - 1; at >= 0; at--)
			{
				int bt = xs.nextInt(at + 1);
				swap(V[at], V[bt]);
				if (used[V[at]]) {
					continue;
				}
				int e = V[at];
				R.push_back(e);

				for (int z = 0; z < k; z++)
				{
					if (gains[e][z] > next_gain) {
						vector<pair<int, int> > T(S);
						T.push_back(make_pair(e, z));

						double H1 = mutual_information(vals, T, k, tick, n);
						double H2 = mutual_information(vals, S, k, tick, n);
						num++;

						double ratio_denom=0.0;      //this is (Cost(new) - Cost(old))

						if(z==0)
							ratio_denom=w1*(pow(A1+1,beta)-pow(A1,beta)); //the rest are cancelled out
						else if(z==1)
							ratio_denom=w2*(pow(A2+1,beta)-pow(A2,beta)); //the rest are cancelled out
						else if(z==2)
							ratio_denom=w3*(pow(A3+1,beta)-pow(A3,beta)); //the rest are cancelled out

						gains[e][z] = (H1 - H2)/ratio_denom;

						if (gains[e][z] > next_gain) {
							next_e = e;
							next_z = z;
							next_gain = gains[e][z];
						}
					}
				}//end for z

				if (R.size()>= ts) {
					break;
				}
			}//end for at

			S.push_back(make_pair(next_e, next_z));
			used[next_e] = true;

			double H = mutual_information(vals, S, k, tick, n);
			if(next_z==0)
				A1++;
			else if(next_z==1)
				A2++;
			else if(next_z==2)
				A3++;

			double cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

			if(H/cost > max_ratio)
			{
				max_H=H;
				max_cost=cost;
				max_ratio = max_H/max_cost;
				size_of_current_best=j+1;
			}

			sum_num += num;

		}//end for j

		JLOG_ADD("sum-num-of-eval", sum_num);
		cout<<"Solution size="<<size_of_current_best<<" H="<<max_H<<" cost="<<max_cost<<" H/cost="<<max_ratio<<endl;
}

//Provided by the authors of http://papers.nips.cc/paper/5709-monotone-k-submodular-function-maximization-with-size-constraints
void single(vector<vector<vector<int> > > &vals, int k, int tick, int n,
		int budget, int sensor, double beta, double w1, double w2, double w3) {

	int B = budget;

	// < gain, < node, tick > >
	priority_queue<pair<double, pair<int, int> > > que;
	for (int v = 0; v < n; v++) {
		que.push(make_pair(1e12, make_pair(v, -1)));
	}

	vector<bool> used(n);
	vector<pair<int, int> > S;
	double psigma = 0;
	int sum_num = 0;

	int A1=0;
	int A2=0;
	int A3=0;
	int size_of_current_best=0;

	for (int j = 0; j < B; j++) {
		pair<int, int> next;

		double start = get_current_time_sec();

		double old_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

		for (int num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<int, int> > pp = que.top();
			que.pop();
			int v = pp.second.first;
			int last = pp.second.second;
			pair<int, int> s = make_pair(v, sensor);
			if (used[v]) {
				continue;
			}
			if (last == j) {
				next = s;
				sum_num += num;
				break;
			}
			vector<pair<int, int> > T(S);
			T.push_back(s);

			double new_denom=0.0;

			if(s.second	==0)
				new_denom=w1*pow(A1+1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);
			else if(s.second==1)
				new_denom=w1*pow(A1,beta)+w2*pow(A2+1,beta)+w3*pow(A3,beta);
			else if(s.second==2)
				new_denom=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3+1,beta);


			double H1 = mutual_information(vals, T, k, tick, n)/new_denom;
			double H2 = mutual_information(vals, S, k, tick, n)/old_denom;


			que.push(make_pair(H1 - H2, make_pair(v, j)));
		}
		S.push_back(next);
		used[next.first] = true;

		//double H = mutual_information(vals, S, k, tick, n);
		if(next.second	==0)
			A1++;
		else if(next.second==1)
			A2++;
		else if(next.second==2)
			A3++;

	}
	JLOG_ADD("sum-num-of-eval", sum_num);

	double H = mutual_information(vals, S, k, tick, n);
	double cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);

	cout<<"Solution size="<<size_of_current_best<<" H="<<H<<" cost="<<cost<<" H/cost="<<H/cost<<endl;
}

void our_random(vector<vector<vector<int> > > &vals, int k, int tick, int n,
		double budget, double beta, double w1, double w2, double w3) {
	int B = budget;

	Xorshift xs(0);
	vector<bool> used(n);
	vector<pair<int, int> > S;

	int A1=0;
	int A2=0;
	int A3=0;

	for (int j = 0; j < B; j++) {
		int e = xs.nextInt(n);
		int z = xs.nextInt(k);
		if (used[e]) {
			j--;
			continue;
		}
		used[e] = true;

		S.push_back(make_pair(e, z));
		if(z==0)
			A1++;
		else if(z==1)
			A2++;
		else if(z==2)
			A3++;

	}

	double cost=w1*pow(A1,beta)+w2*pow(A2,beta)+w3*pow(A3,beta);
	double H=mutual_information(vals, S, k, tick, n);

	cout<<"Solution size="<<B<<" H="<<H<<" cost="<<cost<<" H/cost="<<H/cost<<endl;
}


int main(int argc, char *argv[]) {
	JLOG_INIT(&argc, argv);

	map<string, string> args;
	init_args(argc, argv, args);

	string algo = get_or_die(args, "algo");
	int n = atoi(get_or_die(args, "n").c_str()); // n_pos
	int k = atoi(get_or_die(args, "k").c_str()); // n_sensors
	int tick = atoi(get_or_die(args, "tick").c_str()); // n_ticks
	int budget = atoi(get_or_die(args, "budget").c_str());
	double beta= atof(get_or_die(args, "beta").c_str());

	JLOG_ADD("beta", beta);


	JLOG_PUT("code", algo);

	std::uniform_real_distribution<double> distribution(1,10);
	std::mt19937 gen(1234567);

	double w1=distribution(gen);
	double w2=distribution(gen);
	double w3=distribution(gen);

	//Used - comment out for random
	 w1=5.18658;
	 w2=4.26355;
	 w3=9.38128;

	//Another example 
	/*w1=1;
	w2=1;
	w3=1;*/

	cout<<"w1="<<w1<<" w2="<<w2<<" w3="<<w3<<endl;

	vector<vector<vector<int> > > vals(k);
	for (int s = 0; s < k; s++) {
		char cs[256];
		sprintf(cs, "%d", s);
		ifstream is(get_or_die(args, cs));
		vals[s].resize(tick);
		for (int t = 0; t < tick; t++) {
			vals[s][t].resize(n);
			for (int p = 0; p < n; p++) {
				int v = 0;
				is >> v;
				vals[s][t][p] = v;
			}
		}
		is.close();
	}

	double user_start = get_current_time_sec();
	clock_t cpu_start = clock();

	vector<int> budgets(k, budget);

	if (algo =="k_greed_ratio")
	{
		k_greed_ratio(vals,k, tick, n, budget, beta,w1,w2,w3);
	}
	if (algo == "k_stochastic_greed_ratio") {
			double delta = atof(get_or_die(args, "delta").c_str());
			char cs[256];
			sprintf(cs, "k_stochastic_greed_ratio%d", (int)(-log10(delta)));
			JLOG_PUT("code", cs);

			k_stochastic_greed_ratio(vals, k, tick, n, budget, beta, w1,w2,w3, delta);
	}
	if (algo =="sar"){


			vector<pair<int,int> > sol_g;
			vector<pair<int,int> > sol_h;

			double sol_h_spread=0, sol_g_spread=0;
			double sol_h_cost=0, sol_g_cost=0;

			double g_x=lazy_greedy_sandwich_g(vals, k, tick, n, budget, beta, w1, w2, w3, sol_g, sol_g_spread, sol_g_cost);


			double h_x_h=lazy_greedy_sandwich_h(vals, k, tick, n, budget, beta, w1, w2, w3, sol_h, sol_h_spread, sol_h_cost);

			if(h_x_h <= sol_g_spread/sol_g_cost)
			{
				cout<<endl<<"Solution size="<<sol_g.size()<<" spread="<<sol_g_spread<<" cost="<<sol_g_cost<<" spread/cost="<<sol_g_spread/sol_g_cost<<endl;
			}
			else
				cout<<endl<<"Solution size="<<sol_h.size()<<" spread="<<sol_h_spread<<" cost="<<sol_h_cost<<" spread/cost="<<sol_h_spread/sol_h_cost<<endl;

	}
	if (algo == "single") {
		int topic = 0;
		char cs[256];
		sprintf(cs, "single%d", topic);
		JLOG_PUT("code", cs);
		single(vals, k, tick, n, budget, topic,beta, w1,w2,w3);
	}
	if (algo == "random") {
		our_random(vals,k,tick,n,budget, beta, w1,w2,w3);
	}


	double user_end = get_current_time_sec();
	clock_t cpu_end = clock();

	JLOG_PUT("time.user", user_end - user_start);
	JLOG_PUT("time.cpu", (double) (cpu_end - cpu_start) / CLOCKS_PER_SEC );
}

