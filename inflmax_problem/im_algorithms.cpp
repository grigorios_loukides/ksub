// Copyright 2020, Grigorios Loukides
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Takuya Akiba nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <stack>
#include <algorithm>
#include <time.h> // clock
#include <math.h>
#include <memory> // auto_ptr
#include "jlog.h"
#include "tools.h"
#include "mt19937ar.c"
#include <random>

using namespace std;
using namespace jlog_internal;
typedef long long LL;
typedef unsigned long long ULL;

vector<bool> X;
vector<bool> active;

//Provided by the authors of http://papers.nips.cc/paper/5709-monotone-k-submodular-function-maximization-with-size-constraints
double simulate(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		vector<pair<int, int> > &S, int R) {
	Xorshift xs(0);
	LL sum = 0;
	for (int t = 0; t < R; t++) {
		for (int z = 0; z < k; z++) {
			vector<int> tmp;
			queue<int> Q;
			for (int i = 0; i < S.size(); i++) {
				if (S[i].second == z) {
					Q.push(S[i].first);
					X[S[i].first] = true;
				}
			}
			for (; !Q.empty();) {
				int u = Q.front();
				Q.pop();
				active[u] = true;
				tmp.push_back(u);
				for (int i = 0; i < es[z][u].size(); i++) {
					int v = es[z][u][i].first;
					double p = es[z][u][i].second;
					if (!X[v] && xs.nextDouble() < p) {
						X[v] = true;
						Q.push(v);
					}
				}
			}
			for (int i = 0; i < tmp.size(); i++) {
				X[tmp[i]] = false;
			}
		}
		int n1 = 0;
		for (int v = 0; v < n; v++) {
			if (active[v]) {
				n1++;
				active[v] = false;
			}
		}
		sum += n1;
	}
	return 1.0 * sum / R;
}


void k_greed_ratio(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		int budget, int beta, vector<vector<double> >& costs, double& power) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}

	double total_cost_so_far=0.0;
	double max_ratio=0.0, max_cost=0.0, max_spread=0.0;
	int size_of_current_best=0;
	int global_num=0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	for (int j = 0; j < budget; j++) {
		//printf(INFO "|S| = %d" DEF, j);
		double gain;

		//node, j
		pair<int, int> next;
		double start = get_current_time_sec();

		int num=0;
		for (num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				gain = pp.first;
				//JLOG_ADD("num-of-eval", num);
				break;
			}
			vector<pair<int, int> > SS(S);
			SS.push_back(s);
			double sigma = simulate(n, es, k, SS, beta);
			double psigma = simulate(n, es, k, S, beta);

			int selected_z=pp.second.first.second;
			int selected_node=pp.second.first.first;
			double cost_of_selected_node_at_z=costs[selected_node][selected_z];

			
			que.push(make_pair((sigma - psigma)/( pow(total_cost_so_far+cost_of_selected_node_at_z,power)-pow(total_cost_so_far,power) ), make_pair(s, j)));
		}
		// add a new node 
		S.push_back(next);
		used[next.first] = true;
		

		total_cost_so_far+=costs[next.first][next.second]; 

		double spread_cur = simulate(n,es, k, S, beta); // spread of S (current solution)
		
		
		if(spread_cur / pow(total_cost_so_far,power) > max_ratio)
		{
			max_spread = spread_cur;
			max_cost = pow(total_cost_so_far,power);
			max_ratio = max_spread / max_cost; 
			size_of_current_best=j+1;
		}

		/*JLOG_ADD("seed", next.first);
		JLOG_ADD("item", next.second);
		JLOG_ADD("gain", gain);*/
		global_num+=num;
		 //cout<<"Spread of solution: "<<max_ratio<<" size of sol: "<<size_of_current_best<<"\n";
	}
	cout<<"Solution size="<<size_of_current_best<<" spread="<<max_spread<<" cost="<<max_cost<<" spread/cost="<<max_ratio<<endl;

	cout<<"Number of evaluations: "<<global_num<<endl;
}

double lazy_greedy_sandwich_f(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		int budget, int beta,vector<vector<double> >& costs) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}


	double total_cost_so_far=0.0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	for (int j = 0; j < budget; j++) {
		printf(INFO "|S| = %d" DEF, j);
		double gain;
		pair<int, int> next;
		double start = get_current_time_sec();
		for (int num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				gain = pp.first;
				JLOG_ADD("num-of-eval", num);
				break;
			}
			vector<pair<int, int> > SS(S);
			SS.push_back(s);

			int selected_z=pp.second.first.second;
			int selected_node=pp.second.first.first;
			double cost_of_selected_node_at_z=costs[selected_node][selected_z];
	
			

			double sigma =  pow(total_cost_so_far+cost_of_selected_node_at_z,0.55); //pow( cost of SS )
			double psigma = pow(total_cost_so_far,0.55); //pow( cost of S )
			que.push(make_pair(sigma - psigma, make_pair(s, j)));
		}
		S.push_back(next);
		used[next.first] = true;


		total_cost_so_far+=costs[next.first][next.second]; 

		JLOG_ADD("seed", next.first);
		JLOG_ADD("item", next.second);
		JLOG_ADD("gain", gain);
		
		cout<<"g(x): "<<pow(total_cost_so_far, 0.55)<<" |x|="<<S.size()<<endl;
	}

	
	return pow(total_cost_so_far,0.55);
}

double lazy_greedy_sandwich_g(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		int budget, int beta, vector<vector<double> >& costs, vector<pair<int,int> >& sol, double& sol_spread, double& sol_cost,double& power, int& total_sum_num) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}

	double max_ratio=-1.0;
	
	double total_cost_so_far=0.0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	for (int j = 0; j < budget; j++) {
		//printf(INFO "|S| = %d" DEF, j);
		double gain;
		pair<int, int> next;
		double start = get_current_time_sec();
		for (int num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				gain = pp.first;
				//JLOG_ADD("num-of-eval", num);
				total_sum_num+=num;

				break;
			}
			vector<pair<int, int> > SS(S);
			SS.push_back(s);
			


			double sigma = simulate(n, es, k, SS, beta);
			double psigma = simulate(n, es, k, S, beta);
					
			que.push(make_pair(sigma - psigma, make_pair(s, j)));
		}
		S.push_back(next);
		used[next.first] = true;
	
		total_cost_so_far+=costs[next.first][next.second];

		double cur_infl=simulate(n, es, k, S, beta);
		double cur_cost=pow(total_cost_so_far,power);

		if(cur_infl/cur_cost > max_ratio)
		{
			sol=S;
			sol_spread= cur_infl;
			sol_cost=cur_cost;

		}
		/*JLOG_ADD("seed", next.first);
		JLOG_ADD("item", next.second);
		JLOG_ADD("gain", gain);*/		
		
	}

	//sol=S;
	//sol_spread=simulate(n, es, k, S, beta);
	//sol_cost=pow(total_cost_so_far,power);

	return sol_spread/sol_cost;
}

double lazy_greedy_sandwich_h(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		int budget, int beta,vector<vector<double> >& costs, vector<pair<int,int> >& sol,double& sol_spread, double& sol_cost,double& power,int& total_sum_num) {

	// < gain, < <node,item>, tick > >
	priority_queue<pair<double, pair<pair<int, int>, int> > > que;
	for (int v = 0; v < n; v++) {
		for (int z = 0; z < k; z++) {
			que.push(make_pair(1e12, make_pair(make_pair(v, z), -1)));
		}
	}


	double total_cost_so_far=0.0;
	double max_ratio=0;

	vector<bool> used(n);
	vector<pair<int, int> > S;
	for (int j = 0; j < budget; j++) {
		//printf(INFO "|S| = %d" DEF, j);
		double gain;
		pair<int, int> next;
		double start = get_current_time_sec();
		for (int num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				gain = pp.first;
				//JLOG_ADD("num-of-eval", num);
				total_sum_num+=num;
				break;
			}
			vector<pair<int, int> > SS(S);
			SS.push_back(s);

			int selected_z=pp.second.first.second;
			int selected_node=pp.second.first.first;
			double cost_of_selected_node_at_z=costs[selected_node][selected_z];


			double sigma = simulate(n, es, k, SS, beta)/pow(total_cost_so_far+cost_of_selected_node_at_z,power);
			double psigma = simulate(n, es, k, S, beta)/pow(total_cost_so_far,power);
					
			que.push(make_pair(sigma - psigma, make_pair(s, j)));
		}
		S.push_back(next);
		used[next.first] = true;
	
		total_cost_so_far+=costs[next.first][next.second]; 


		 double cur_infl=simulate(n, es, k, S, beta);
                double cur_cost=pow(total_cost_so_far,power);

                if(cur_infl/cur_cost > max_ratio)
                {
                        sol=S;
                        sol_spread= cur_infl;
                        sol_cost=cur_cost;

                }

	
		
		
	}
	
	return  sol_spread/sol_cost;

}


void k_stochastic_greed_ratio(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		int budget, int beta, double delta, vector<vector<double> >& costs, double& power) {
	vector<int> V(n);
	for (int i = 0; i < n; i++) {
		V[i] = i;
	}
	Xorshift xs(0);
	vector<bool> used(n);
	vector<pair<int, int> > S;

	// <node, item> -> gain
	vector<vector<double> > gains(n);
	for (int i = 0; i < n; i++) {
		gains[i].resize(k);
		gains[i].assign(k, 1e12);
	}

	double total_cost_so_far=0.0;
	double max_ratio=0.0, max_spread=0.0, max_cost=0.0;
	int size_of_current_best=0;

	int sample_is_V=0;
	int global_num=0;

	double psigma = 0;
	for (int j = 0; j < budget; j++) {
		//printf(INFO "|S| = %d" DEF, j);

		vector<int> R;

		//int ts = 1.0 * (n - j) / (budget - j) * log(budget / delta);
		//In our case the sample size is different

		int ts=-1;

		if(log(budget/delta)>=budget)
		{
			ts=n;
			sample_is_V++;
		}
		else
			ts=1.0*ceil(log(budget/delta));
		
		int at = n - 1;
		for (int i = 0; i < ts && at >= 0; i++, at--) {
			int bt = xs.nextInt(at + 1);
			swap(V[at], V[bt]);
			if (used[V[at]]) {
				i--;
				continue;
			}
			R.push_back(V[at]);
		}

		// < gain, < <node,item>, tick > >
		priority_queue<pair<double, pair<pair<int, int>, int> > > que;
		for (int i = 0; i < R.size(); i++) {
			int v = R[i];
			for (int z = 0; z < k; z++) {
				que.push(
						make_pair(gains[v][z], make_pair(make_pair(v, z), -1)));
			}
		}

		double gain;
		pair<int, int> next;
		double start = get_current_time_sec();

		int num=0;
		for (num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<pair<int, int>, int> > pp = que.top();
			que.pop();
			pair<int, int> s = pp.second.first;
			int last = pp.second.second;
			if (used[s.first]) {
				continue;
			}
			if (last == j) {
				next = s;
				gain = pp.first;
				//JLOG_ADD("num-of-eval", num);
				break;
			}
			vector<pair<int, int> > SS(S);
			SS.push_back(s);
			double sigma = simulate(n, es, k, SS, beta);
			double psigma = simulate(n, es, k, S, beta);

			int selected_z=pp.second.first.second;
			int selected_node=pp.second.first.first;
			double cost_of_selected_node_at_z=costs[selected_node][selected_z];
			
		
			gains[s.first][s.second] = (sigma - psigma)/(pow(total_cost_so_far+cost_of_selected_node_at_z,power)-pow(total_cost_so_far,power));

			que.push(make_pair(gains[s.first][s.second], make_pair(s, j)));
		}
		S.push_back(next);
		used[next.first] = true;

		total_cost_so_far+=costs[next.first][next.second]; 

		double spread_cur = simulate(n,es, k, S, beta); // spread of S (current solution)
		
		//cout<<"TC: "<<total_cost_so_far<<" SP: "<<spread_cur<<" "<<spread_cur / pow(total_cost_so_far,0.65)<<endl;
		
		if(spread_cur / pow(total_cost_so_far,power) > max_ratio)
		{
			max_spread= spread_cur;
			max_cost= pow(total_cost_so_far,power);
			max_ratio = max_spread/ max_cost;
			size_of_current_best=j+1;
		}

		/*JLOG_ADD("seed", next.first);
		JLOG_ADD("item", next.second);
		JLOG_ADD("gain", gain);*/
		global_num+=num;
	}
	cout<<"Solution size="<<size_of_current_best<<" spread="<<max_spread<<" cost="<<max_cost<<" spread/cost="<<max_ratio<<"\n";
	cout<<"Sample is V: "<<sample_is_V;
	cout<<"Number of evaluations:"<<global_num<<endl;
}

//Provided by the authors of http://papers.nips.cc/paper/5709-monotone-k-submodular-function-maximization-with-size-constraints
void single(int n, vector<vector<vector<pair<int, double> > > > &es, int k,
		int budget, int beta, int topic, vector<vector<double> >& costs,double& power) {
	// < gain, < node, tick > >
	priority_queue<pair<double, pair<int, int> > > que;
	for (int v = 0; v < n; v++) {
		que.push(make_pair(1e12, make_pair(v, -1)));
	}

	vector<bool> used(n);
	vector<pair<int, int> > S;
	double psigma = 0;
	double total_cost_so_far=0;

	for (int j = 0; j < budget; j++) {
		//printf(INFO "|S| = %d" DEF, j);

		double gain = 0;
		pair<int, int> next;
		double start = get_current_time_sec();
		for (int num = 0;; num++) {
			double now = get_current_time_sec();

			pair<double, pair<int, int> > pp = que.top();
			que.pop();
			int v = pp.second.first;
			int last = pp.second.second;
			pair<int, int> s = make_pair(v, topic);
			if (used[v]) {
				continue;
			}
			if (last == j) {
				next = s;
				gain = pp.first;
				//JLOG_ADD("num-of-eval", num);
				break;
			}
			vector<pair<int, int> > SS(S);
			SS.push_back(s);


			double cost_of_selected_node_at_z=costs[v][last];


			double sigma = simulate(n, es, k, SS, beta)/pow(total_cost_so_far+cost_of_selected_node_at_z,power);
			double psigma = simulate(n, es, k, S, beta)/pow(total_cost_so_far,power);
					
			que.push(make_pair(sigma - psigma, make_pair(v, j)));

			
			//original single based on spread
			/*double sigma = simulate(n, es, k, SS, beta);
			double psigma = simulate(n, es, k, S, beta);
			que.push(make_pair(sigma - psigma, make_pair(v, j)));*/
		}
		S.push_back(next);
		used[next.first] = true;

		/*JLOG_ADD("seed", next.first);
		JLOG_ADD("item", next.second);

		JLOG_ADD("gain", gain);*/

		total_cost_so_far+=costs[next.first][next.second]; 
	}
		double sol_spread=simulate(n, es, k, S, beta);
		double sol_cost=pow(total_cost_so_far, power);

		cout<<"Solution size="<<S.size()<<" spread="<<sol_spread<<" cost="<<sol_cost<<" spread/cost="<<sol_spread/sol_cost<<endl;
}

/*  How to run the algorithms

./im_algorithms -algo=k_greed_ratio -graph=digg -edge=./dat/digg_graph_100.tsv -prob=./dat/digg_prob_100_1000_pow10.tsv -k=3 -budget=3523 -beta=100 -delta=0.1 -power=0.5 -topic=1

./im_algorithms -algo=k_stochastic_greed_ratio -graph=digg -edge=./dat/digg_graph_100.tsv -prob=./dat/digg_prob_100_1000_pow10.tsv -k=3 -budget=3523 -beta=100 -delta=0.1 -power=0.5 -topic=1

./im_algorithms -algo=sar -graph=digg -edge=./dat/digg_graph_100.tsv -prob=./dat/digg_prob_100_1000_pow10.tsv -k=3 -budget=3523 -beta=100 -delta=0.1 -power=0.5 -topic=1

// the topic here says at which dimension we will put all elements. It is at most k-1
./im_algorithms -algo=single -graph=digg -edge=./dat/digg_graph_100.tsv -prob=./dat/digg_prob_100_1000_pow10.tsv -k=3 -budget=3523 -beta=100 -delta=0.1 -power=0.5 -topic=1  


./im_algorithms -algo=random -graph=digg -edge=./dat/digg_graph_100.tsv -prob=./dat/digg_prob_100_1000_pow10.tsv -k=3 -budget=3523 -beta=100 -delta=0.1 -power=0.5 -topic=1

./im_algorithms -algo=degree -graph=digg -edge=./dat/digg_graph_100.tsv -prob=./dat/digg_prob_100_1000_pow10.tsv -k=3 -budget=3523 -beta=100 -delta=0.1 -power=0.5 -topic=1

*/

int main(int argc, char *argv[]) {
	JLOG_INIT(&argc, argv);

	map<string, string> args;
	init_args(argc, argv, args);

	string algo = get_or_die(args, "algo");
	string graph = get_or_die(args, "graph");
	string edge = get_or_die(args, "edge");
	string prob = get_or_die(args, "prob");
	int k = atoi(get_or_die(args, "k").c_str());
	int budget = atoi(get_or_die(args, "budget").c_str());

	double power= atof(get_or_die(args, "power").c_str());

	int beta = atoi(get_or_die(args, "beta").c_str());
	JLOG_ADD("power", power);

	JLOG_PUT("code", algo);

	vector<vector<pair<int, vector<double> > > > es0 = load_graph(edge, prob,
			k);
	int n = es0.size();


	vector<vector<vector<pair<int, double> > > > es(k);
	for (int z = 0; z < k; z++) {
		es[z].resize(n);
		for (int u = 0; u < n; u++) {
			for (int i = 0; i < es0[u].size(); i++) {
				int v = es0[u][i].first;
				double p = es0[u][i].second[z];
				es[z][u].push_back(make_pair(v, p));
			}
		}
	}

	

	X.resize(n);
	X.assign(n, false);
	active.resize(n);
	active.assign(n, false);

        vector<vector<double> > costs;

         /* seed the PRNG (MT19937) using a fixed value (in our case, 1234567) */
	std::mt19937 gen(1234567);
	


	//2000 is the minimum cost, 20000 is the maximum
	std::uniform_real_distribution<double> distribution(2000,20000);

	for(int i=0; i<n; ++i)
	{
		vector<double> node_i_costs;
		for(int j=0;j<k; ++j)
			node_i_costs.push_back(distribution(gen));		
			
		costs.push_back(node_i_costs);
	}


	double user_start = get_current_time_sec();
	clock_t cpu_start = clock();

	if (algo == "k_greed_ratio") {

		k_greed_ratio(n, es, k, budget, beta, costs, power);
	}

	if (algo =="sar"){


		vector<pair<int,int> > sol_g;
		vector<pair<int,int> > sol_h;	

		double sol_h_spread=0, sol_g_spread=0;
		double sol_h_cost=0, sol_g_cost=0;

		int total_sum_num=0;
		double g_x=lazy_greedy_sandwich_g(n, es,k, budget, beta, costs, sol_g, sol_g_spread, sol_g_cost,power,total_sum_num);
		

		double h_x_h=lazy_greedy_sandwich_h(n, es,k, budget, beta, costs, sol_h, sol_h_spread, sol_h_cost,power,total_sum_num);
		
		if(h_x_h <= sol_g_spread/sol_g_cost)
		{
			cout<<endl<<"Solution size="<<sol_g.size()<<" spread="<<sol_g_spread<<" cost="<<sol_g_cost<<" spread/cost="<<sol_g_spread/sol_g_cost<<endl;
		}	 
		else
			cout<<endl<<"Solution size="<<sol_h.size()<<" spread="<<sol_h_spread<<" cost="<<sol_h_cost<<" spread/cost="<<sol_h_spread/sol_h_cost<<endl;

		cout<<endl<<"# of func eval: "<<total_sum_num<<endl;

	}

	if (algo == "k_stochastic_greed_ratio") {
		double delta = atof(get_or_die(args, "delta").c_str());
		char cs[256];
		sprintf(cs, "k_stochastic_greed_ratio%d", (int)(-log10(delta)));
		JLOG_PUT("code", cs);
					
		k_stochastic_greed_ratio(n, es, k, budget, beta, delta, costs,power);
	}
	if (algo == "single") {
		int topic = atoi(get_or_die(args, "topic").c_str());
		char cs[256];
		sprintf(cs, "single%d", topic);
		JLOG_PUT("code", cs);
		single(n, es, k, budget, beta, topic,costs,power);
	}

	if (algo == "random") {
		Xorshift xs(0);
		vector<pair<int,int> > sol;
		double total_cost=0.0;
		for (int i = 0; i < budget; i++) {
			int v = xs.nextInt(n);
			int z = xs.nextInt(k);			
			sol.push_back(make_pair(v,z));

			total_cost+=costs[v][z];
		}
		double pow_cost=pow(total_cost,power);
		double sol_spread=simulate(n, es, k, sol, beta);
		cout<<endl<<"Solution size="<<sol.size()<<" spread="<<sol_spread<<" cost="<<pow_cost<<" spread/cost="<<sol_spread/pow_cost<<endl;
	}
	if (algo == "degree") {
		Xorshift xs(0);
		vector<pair<int, int> > odeg;
		vector<pair<int,int> > sol;
		double total_cost=0.0;
		for (int v = 0; v < n; v++) {
			int o = es[v].size();
			odeg.push_back(make_pair(-o, v));
		}
		sort(odeg.begin(), odeg.end());
		for (int i = 0; i < budget; i++) {
			int v = odeg[i].second;
			int z = xs.nextInt(k);
			sol.push_back(make_pair(v,z));

			total_cost+=costs[v][z];
		}
		double pow_cost=pow(total_cost,power);
		double sol_spread=simulate(n, es, k, sol, beta);
		cout<<endl<<"Solution size="<<sol.size()<<" spread="<<sol_spread<<" cost="<<pow_cost<<" spread/cost="<<sol_spread/pow_cost<<endl;
	}

	double user_end = get_current_time_sec();
	clock_t cpu_end = clock();

	JLOG_PUT("time.user", user_end - user_start);
	JLOG_PUT("time.cpu", (double) (cpu_end - cpu_start) / CLOCKS_PER_SEC );
}

